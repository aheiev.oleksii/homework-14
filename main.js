const array = [1, 2, 3, 4, 5, 6, 7, -2];

function removeElement(arr, num) {
  let index = arr.indexOf(num);
  arr.splice(index, 1);
  return arr;
}

console.log(removeElement(array, 5));
